import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    shadow: {
        elevation: 3,
        borderColor: 'darkgrey',
        borderBottomWidth: 0,
        shadowColor: 'darkgrey',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
});

export default styles;